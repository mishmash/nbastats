const nbaplayer = require('./nbaplayer');

(async function () {
    const args = process.argv.slice (2);
    if (args.length == 0) {
        console.log ('Usage: node nbastats.js <player-name>');
        return;
    }
    let player = args[0];

    try {
        rows = await nbaplayer.stats(player, 'throw');

        if (rows.length == 0) {
            console.log("Nothing to display.");
            return;
        }
        // Print out <season> <3pa> and done
        rows.forEach(function(entry) {
            console.log(entry.season + ' ' + entry.tpa);
        });
    }
    catch (exception) {
        console.log('Error: ' + exception);
    }
}());
