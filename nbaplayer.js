const puppeteer = require('puppeteer');

module.exports = {
    // User agent
    userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' +
                'AppleWebKit/537.36 (KHTML, like Gecko) ' +
                'Chrome/61.0.3163.100 Safari/537.36',

    browser: null,

    /**
     * Gets the 3PA stats for a given player
     * @param {string} player name
     * @param {string} multiple what to do when multiple players match the name,
     * 'first' picks the first player, 'throw' throws an error
     */
    stats: async function (name, multiple)
    {
        try {
            return await this._stats(name, multiple);
        }
        catch (exception) {
            await this.browser.close();
            throw exception;
        }
        finally {
            await this.browser.close();
        }
    },

    /**
     * Wrapper for the public stats function, so we don't need to call
     * browser.close throughout, but rather just throw
     */
    _stats: async function (name, multiple)
    {
        multiple = multiple || 'first';

        this.browser = await puppeteer.launch();
        const page = await this.browser.newPage();

        // When using default UA String things are kinda flaky,
        // desktop UA works better
        await page.setUserAgent (this.userAgent);

        await page.goto('https://stats.nba.com');

        // Search for the player
        await page.click('a.stats-search__icon');
        await page.type('.stats-search__input', name);

        // Get all players matching query
        // await page.waitFor('.stats-search__link-anchor');
        const nodes = await page.evaluate(function() {
            return Array.from (
                document.querySelectorAll('.stats-search__link-anchor'),
                function(a) { return a.href }
            )
        });

        if (nodes.length == 0) {
            throw 'No players matching "' + name + '" were found';
        }
        else if (nodes.length > 1 && multiple == 'throw') {
            throw 'Multiple players matching "' + name + '"';
        }
        // Load player stats page with SeasonType=Regular Season
        // and PerMode=Per40
        const queryString = '?SeasonType=Regular Season&PerMode=Per40';
        await page.goto(nodes[0] + queryString, {waitUntil: 'networkidle2'});

        // Probably not needed since we already wait for networkidle2
        await page.waitFor('nba-stat-table');

        // Gets 3PA column index, possibly a bit redundant, a static
        // nth-child(10) would also work
        const tpaIndex = await page.evaluate (function () {
            const ths = Array.from(document.querySelectorAll('nba-stat-table[template="player/player-traditional"] > div.nba-stat-table > div.nba-stat-table__overflow > table > thead th'));
            for (let i = 0; i < ths.length; i++) {
                if (ths[i].textContent.trim() == "3PA") {
                    return i + 1;
                }
            }
            return null;
        });

        // Something is (probably) wrong
        if (tpaIndex === null) {
            throw 'Something is missing in the stats table';
        }

        // Get all the rows in the table body and map them by season
        const rows = await page.evaluate (function (index) {
            const trs = Array.from(document.querySelectorAll('nba-stat-table[template="player/player-traditional"] > div.nba-stat-table > div.nba-stat-table__overflow > table > tbody > tr'));
            return trs.map(function (tr) {
                return {
                    season: tr.querySelector('td:first-child').textContent.trim(),
                    tpa:    tr.querySelector('td:nth-child(' + index + ')').textContent.trim()
                };
            });
        }, tpaIndex);
        return rows;
    }
}
