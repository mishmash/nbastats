NBA Stats
=========
Fetches 3-pointers average (per 40 min) player statistic from `https://stats.nba.com`
and displays them on a per season basis.

Requirements
============
To run you need `Puppeteer` installed.

Usage Example
============
Usage: `node nbastats.js <player-name>`

Example:
```
node nbastats.js "LeBron James"
```

Outputs (in time of writing):
```
2019-20 7.1
2018-19 6.8
2017-18 5.4
2016-17 4.9
2015-16 4.2
2014-15 5.4
2013-14 4.2
2012-13 3.5
2011-12 2.6
2010-11 3.6
2009-10 5.2
2008-09 5.0
2007-08 4.7
2006-07 3.9
2005-06 4.5
2004-05 3.6
2003-04 2.8
```
